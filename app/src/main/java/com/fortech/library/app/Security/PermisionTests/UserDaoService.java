package com.fortech.library.app.Security.PermisionTests;

import com.fortech.library.impl.dao.user.User;
import com.fortech.library.impl.dao.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.fortech.library.app.Security.UserRole.ADMIN;
import static com.fortech.library.app.Security.UserRole.CLIENT;

@Repository
public class UserDaoService {

    private final PasswordEncoder passwordEncoder;

    //add userRepository here -->
    @Autowired
    UserRepository userRepository;


    @Autowired
    public UserDaoService(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Optional<SecurityUser> selectUserByUsername(String username) {
        return getUsers().stream().filter(SecurityUser -> username.equals(SecurityUser.getUsername()))
                .findFirst();
    }

    private List<SecurityUser> getUsers() {
        List<SecurityUser> securityUsers = new ArrayList<>();
        Iterable<User> list = userRepository.findAll();

        list.forEach(user -> {
            String role = user.getUserRole().toUpperCase();
            String username = user.getUsername();

            if (role.equals("CLIENT")) {
                securityUsers.add(new SecurityUser(CLIENT.getGrantedAuthority(),
                        passwordEncoder.encode(user.getPassword()),
                        username,
                        true,
                        true,
                        true,
                        true));
            } else if (role.equals("ADMIN")) {
                securityUsers.add(new SecurityUser(ADMIN.getGrantedAuthority(),
                        passwordEncoder.encode(user.getPassword()),
                        user.getUsername(),
                        true,
                        true,
                        true,
                        true));
            }
        });
        return securityUsers;
    }
}
