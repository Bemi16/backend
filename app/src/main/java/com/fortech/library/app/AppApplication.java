package com.fortech.library.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"com.fortech", "com.fortech.library.app.Security"})
@EnableElasticsearchRepositories("com.fortech.library.impl.elasticsearch")
@EnableJpaRepositories({"com.fortech.library.impl.dao.book", "com.fortech.library.impl.dao.author", "com.fortech.library.impl.dao.user"})
@EntityScan({"com.fortech.library.impl.dao.book", "com.fortech.library.impl.dao.author", "com.fortech.library.impl.dao.user"})
public class AppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);
    }
}
