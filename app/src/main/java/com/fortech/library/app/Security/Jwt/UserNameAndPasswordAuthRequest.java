package com.fortech.library.app.Security.Jwt;

public class UserNameAndPasswordAuthRequest {

    private String username;
    private String password;

    public UserNameAndPasswordAuthRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
