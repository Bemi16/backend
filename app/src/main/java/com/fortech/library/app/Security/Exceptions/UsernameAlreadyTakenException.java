package com.fortech.library.app.Security.Exceptions;

public class UsernameAlreadyTakenException extends Exception {

    public UsernameAlreadyTakenException(String message) {
        super(message);
    }
}
