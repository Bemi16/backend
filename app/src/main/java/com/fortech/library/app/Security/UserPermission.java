package com.fortech.library.app.Security;

public enum UserPermission {

    BOOK_READ("book:read"),
    BOOK_ADD("book:add"),
    BOOK_RENT("book:rent"),
    BOOK_DELETE("book:delete"),
    CLIENT_READ("client:read"),
    CLIENT_UPDATE("client:update");

    private final String permission;


    UserPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
