package com.fortech.library.app.Security;

import com.google.common.collect.Sets;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.fortech.library.app.Security.UserPermission.*;


public enum UserRole {

    CLIENT(Sets.newHashSet(BOOK_READ, BOOK_RENT)),
    ADMIN(Sets.newHashSet(BOOK_READ, BOOK_DELETE, BOOK_ADD, CLIENT_READ, CLIENT_UPDATE));


    private final Set<UserPermission> permissions;

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthority() {
        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());

        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return permissions;
    }
}
