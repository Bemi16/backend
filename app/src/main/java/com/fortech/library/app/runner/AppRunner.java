package com.fortech.library.app.runner;

import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.exceptions.UserNotFoundException;
import com.fortech.library.impl.services.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {

    @Autowired
    private UserService service;

    private void saveDefaultAdminUser() {
        try {
            service.deleteUserByUsername("admin");
        } catch (UserNotFoundException ex) {
            ex.getMessage();
        }
        UserDto user = new UserDto();
        user.setFirstName("admin");
        user.setLastName("admin");
        user.setEmail("admin@admin.com");
        user.setUsername("admin");
        user.setPassword("admin");
        user.setUserRole("ADMIN");
        service.addUser(user);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        saveDefaultAdminUser();
    }
}
