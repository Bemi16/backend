package com.fortech.library.app.Security.PermisionTests;

import com.fortech.library.app.Security.Exceptions.UsernameAlreadyTakenException;
import com.fortech.library.impl.dao.user.User;
import com.fortech.library.impl.dao.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    private final UserDaoService userDao;

    @Autowired
    UserRepository userRepository;

    @Autowired
    public UserService(UserDaoService userDao, UserRepository userRepository) {
        this.userRepository = userRepository;
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDao.selectUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("UserName %s not found", username)));
    }

    public User save(User user) throws UsernameAlreadyTakenException {
        Optional<User> newUser = userRepository.findByUsername(user.getUsername());

        if (newUser.isPresent()) {
            throw new UsernameAlreadyTakenException("Username already taken!");
        } else {
            userRepository.save(user);
        }
        return user;
    }
}
