CREATE TABLE books (
    b_id INTEGER PRIMARY KEY NOT NULL,
    b_title VARCHAR(255),
    b_publisher VARCHAR(255),
    b_category VARCHAR(255),
    a_id INTEGER NOT NULL,
    CONSTRAINT a_id_fk FOREIGN KEY (a_id) REFERENCES authors(a_id)
);

CREATE TABLE authors (
    a_id INTEGER PRIMARY KEY NOT NULL,
    a_name VARCHAR(255),
    a_biography VARCHAR(255)
);

CREATE TABLE securityUsers (
    u_id INTEGER PRIMARY KEY NOT NULL,
    u_firstName VARCHAR(255),
    u_lastName VARCHAR(255),
    u_username VARCHAR(255),
    u_password VARCHAR(255),
    u_userRole VARCHAR(255),
    u_email VARCHAR(255)
);

CREATE TABLE users_books (
    u_id INTEGER NOT NULL,
    b_id INTEGER NOT NULL,
    CONSTRAINT u_id_fk FOREIGN KEY (u_id) REFERENCES securityUsers(u_id),
    CONSTRAINT b_id_fk FOREIGN KEY (b_id) REFERENCES books(b_id)
);