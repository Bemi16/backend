package com.fortech.library.app.Security.PermisionTests;

import com.fortech.library.app.Security.UserPermission;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;

import static com.fortech.library.app.Security.UserRole.ADMIN;
import static com.fortech.library.app.Security.UserRole.CLIENT;
import static org.junit.jupiter.api.Assertions.assertEquals;


class SecurityUserRoleTest {


    @Test
    void givenAllListOfPermisions_ShouldReturnAllPermisions() {
        Set<UserPermission> result = ADMIN.getPermissions();
        Set<UserPermission> resPerm = CLIENT.getPermissions();
        Set<SimpleGrantedAuthority> resAuthClient = CLIENT.getGrantedAuthority();
        Set<SimpleGrantedAuthority> resAuthADMIN = ADMIN.getGrantedAuthority();

        assertEquals(result.size(), 5);
        assertEquals(resPerm.size(), 2);
        assertEquals(resAuthClient.size(), 3);
        assertEquals(resAuthADMIN.size(), 6);
    }

}