package com.fortech.library.app.Security.PermisionTests;

import com.fortech.library.app.Security.Exceptions.UsernameAlreadyTakenException;
import com.fortech.library.app.Security.PasswordConfig;
import com.fortech.library.impl.dao.user.User;
import com.fortech.library.impl.dao.user.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.fortech.library.app.Security.UserRole.ADMIN;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;


class SecurityUserServiceTest {

    PasswordConfig passwordConfig = new PasswordConfig();

    @Mock
    UserRepository userRepositor = mock(UserRepository.class);

    private final UserDaoService userDao = new UserDaoService(passwordConfig.passwordEncoder(), userRepositor);

    @Autowired
    private final UserService UserService = new UserService(userDao, userRepositor);


//    @Test
//    void givenCLientWithROleClient_ShouldReturnACLientUser() {
//        String passsword = passwordConfig.passwordEncoder().encode("password");
//        String username = "serban";
//        UserDetails user = new SecurityUser(
//                CLIENT.getGrantedAuthority(),
//                passsword,
//                username,
//                true,
//                true,
//                true,
//                true);
//
//
//            var auth =  user.getAuthorities();
//            var password =  user.getPassword();
//        var isExpired = user.isAccountNonExpired();
//        var isNotLocked =  user.isAccountNonLocked();
//        var isCredentialNonExpired =   user.isCredentialsNonExpired();
//        var isEnabled =  user.isEnabled();
//
//        Optional<UserDetails> demoUser = Optional.of(user);
//
//        List<User> list = new ArrayList<>();
//
//        list.add(new User("Serban","Serban","serban","Serban","client","email",null));
//
//        doReturn(demoUser).when(userRepositor).findByUsername(username);
//        doReturn(list).when(userRepositor).findAll();
//        UserDetails result = UserService.loadUserByUsername(username);
//
//
//        assertEquals(result.getUsername(),user.getUsername());
//        assertEquals(auth,result.getAuthorities());
//        assertEquals(password,user.getPassword());
//        assertEquals(isExpired,true);
//        assertEquals(isNotLocked,true);
//        assertEquals(isCredentialNonExpired,true);
//        assertEquals(isEnabled,true);
//
//    }

    @Test
    void givenUserWithRoleADMIN_ShouldReturnAdminUser() {
        String passsword = passwordConfig.passwordEncoder().encode("password");
        String username = "serban";
        UserDetails user = new SecurityUser(ADMIN.getGrantedAuthority(),
                passsword,
                username,
                true,
                true,
                true,
                true);
        Optional<UserDetails> demoUser = Optional.of(user);

        List<User> list = new ArrayList<>();

        list.add(new User("Serban", "Serban", "serban", "Serban", "admin", "email", null));


        doReturn(demoUser).when(userRepositor).findByUsername(username);

        doReturn(list).when(userRepositor).findAll();
        UserDetails result = UserService.loadUserByUsername(username);

        assertEquals(result.getAuthorities(), user.getAuthorities());
    }

    @Test
    void givenUserThatExist_ShouldReturnUserAlreadyExistException() {
        String username = "serban";
        User newUser = new User("Serban", "serban", "serban", "Serban", "client", "email", null);
        UserDetails user = new SecurityUser(ADMIN.getGrantedAuthority(),
                "passsword",
                username,
                true,
                true,
                true,
                true);
        Optional<UserDetails> demoUser = Optional.of(user);

        doReturn(demoUser).when(userRepositor).findByUsername("serban");

        Exception exception = assertThrows(UsernameAlreadyTakenException.class, () -> {
            UserService.save(newUser);
        });

        String expectedMessage = "Username already taken!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void givenUser_ShouldSaveNewUser() throws UsernameAlreadyTakenException {
        String username = "serban";
        User newUser = new User("Serban", "Serban", "Serban", "Serban", "client", "email", null);


        Optional<UserDetails> demoUser = Optional.empty();

        doReturn(demoUser).when(userRepositor).findByUsername("serban");

        User result = UserService.save(newUser);

        assertEquals(result.getUsername(), newUser.getUsername());


    }
}
