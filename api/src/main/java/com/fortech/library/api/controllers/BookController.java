package com.fortech.library.api.controllers;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.api.dto.book.BookDto;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_CLIENT')")
@RequestMapping("/view/authors")
public interface BookController {

    @GetMapping("/books/page/{pageNumber}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<Page<BookDto>> getAllBooksForAllAuthors(@PathVariable(value = "pageNumber") Integer pageNumber);

    @GetMapping("/{authorName}/books")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<List<BookDto>> getAllBooksForAuthor(@PathVariable(value = "authorName") String authorName);

    @GetMapping("/books/{bookTitle}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<AuthorDto> getAuthorForBook(@PathVariable(value = "bookTitle") String bookTitle);

    @PostMapping("/{authorName}/books")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<BookDto> addBookToAuthor(@RequestBody BookDto book, @PathVariable(value = "authorName") String authorName);

    @PutMapping("/{authorName}/books/{bookTitle}")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<BookDto> updateBookForAuthor(@PathVariable(value = "authorName") String authorName, @PathVariable(value = "bookTitle") String bookTitle, @RequestBody BookDto book);

    @DeleteMapping("/books/{bookTitle}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<String> deleteBookByTitleFromAuthor(@PathVariable(value = "bookTitle") String bookTitle);

}
