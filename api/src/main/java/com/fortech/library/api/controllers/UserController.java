package com.fortech.library.api.controllers;

import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.api.dto.user.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@CrossOrigin
@RequestMapping("/view")
public interface UserController {

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/users/page/{pageNumber}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<Page<UserDto>> getAllUsers(@PathVariable(value = "pageNumber") Integer pageNumber);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/users/{username}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<UserDto> getUserByUsername(@PathVariable(value = "username") String username);

    @PostMapping("/users")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<UserDto> createUser(@RequestBody UserDto user);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("/users/{username}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<String> deleteUserByUsername(@PathVariable("username") String username);

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/users/{username}")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<UserDto> updateUser(@PathVariable(value = "username") String username, @RequestBody UserDto user);

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_CLIENT')")
    @PostMapping("/users/{username}/loanbook/{bookTitle}")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<UserDto> loanBookUser(@PathVariable("username") String username, @PathVariable("bookTitle") String bookTitle);

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_CLIENT')")
    @PostMapping("/users/{username}/returnbook/{bookTitle}")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<UserDto> returnBookUser(@PathVariable("username") String username, @PathVariable("bookTitle") String bookTitle);

    @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_CLIENT')")
    @GetMapping("/users/{username}/books")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<Set<BookDto>> getAllLoanedBooksForUser(@PathVariable("username") String username);
}
