package com.fortech.library.api.dto.book;

public class BookDto {

    private String title;
    private String publisher;
    private String category;
    private int stock;

    public BookDto() {
    }

    public BookDto(String title, String publisher, String category, int stock) {
        this.title = title;
        this.publisher = publisher;
        this.category = category;
        this.stock = stock;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
