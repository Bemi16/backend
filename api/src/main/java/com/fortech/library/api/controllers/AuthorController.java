package com.fortech.library.api.controllers;

import com.fortech.library.api.dto.author.AuthorDto;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/view")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public interface AuthorController {
    @GetMapping("/authors/page/{pageNumber}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<Page<AuthorDto>> getAllAuthors(@PathVariable(value = "pageNumber") Integer pageNumber);

    @GetMapping("/authors/{authorName}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<AuthorDto> getAuthorByName(@PathVariable(value = "authorName") String authorName);

    @PostMapping("/authors")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<AuthorDto> addAuthor(@RequestBody AuthorDto authorDto);

    @PutMapping("/authors/{authorName}")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<AuthorDto> updateAuthor(@PathVariable(value = "authorName") String authorName, @RequestBody AuthorDto authorDto);

    @DeleteMapping("/authors/{authorName}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<String> deleteAuthorByName(@PathVariable(value = "authorName") String authorName);

}
