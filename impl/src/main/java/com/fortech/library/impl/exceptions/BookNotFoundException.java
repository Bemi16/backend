package com.fortech.library.impl.exceptions;

public class BookNotFoundException extends RuntimeException {
    public BookNotFoundException(String bookTitle) {
        super("Could not find the book with title: " + bookTitle);
    }
}
