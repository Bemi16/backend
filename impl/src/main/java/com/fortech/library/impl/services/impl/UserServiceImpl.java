package com.fortech.library.impl.services.impl;

import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.dao.book.Book;
import com.fortech.library.impl.dao.book.BookRepository;
import com.fortech.library.impl.dao.user.User;
import com.fortech.library.impl.dao.user.UserRepository;
import com.fortech.library.impl.elasticsearch.BookElasticsearchRepository;
import com.fortech.library.impl.elasticsearch.UserElasticsearchRepository;
import com.fortech.library.impl.exceptions.BookNotFoundException;
import com.fortech.library.impl.exceptions.OutOfStockException;
import com.fortech.library.impl.exceptions.UserNotFoundException;
import com.fortech.library.impl.exceptions.UsernameAlreadyExists;
import com.fortech.library.impl.mapper.book.api.BookMapper;
import com.fortech.library.impl.mapper.user.api.UserMapper;
import com.fortech.library.impl.services.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final BookRepository bookRepository;
    private final UserMapper userMapper;
    private final BookMapper bookMapper;
    private final UserElasticsearchRepository userElasticsearchRepository;
    private final BookElasticsearchRepository bookElasticsearchRepository;
    private final ElasticsearchOperations elasticsearchOperations;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BookRepository bookRepository, UserMapper userMapper, BookMapper bookMapper, UserElasticsearchRepository userElasticsearchRepository, BookElasticsearchRepository bookElasticsearchRepository, ElasticsearchOperations elasticsearchOperations) {
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
        this.userMapper = userMapper;
        this.bookMapper = bookMapper;
        this.userElasticsearchRepository = userElasticsearchRepository;
        this.bookElasticsearchRepository = bookElasticsearchRepository;
        this.elasticsearchOperations = elasticsearchOperations;
    }

    @Override
    public Page<UserDto> getAllUsers(Integer pageNumber) {
        Pageable pageable = createPageRequest(pageNumber);

        Page<User> users = userElasticsearchRepository.findAll(pageable);

        return new PageImpl<>(
                users.getContent().stream()
                        .map(u -> userMapper.toUserDto(u))
                        .collect(Collectors.toList()),
                pageable, users.getTotalElements()
        );
    }

    private Pageable createPageRequest(Integer pageNumber) {
        return PageRequest.of(pageNumber, 20);
    }

    @Override
    public UserDto getUserByUsername(String username) throws UserNotFoundException {
        User user = userElasticsearchRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        return userMapper.toUserDto(user);
    }

    @Override
    public UserDto addUser(UserDto userDto) throws UsernameAlreadyExists {
        if (userRepository.findByUsername(userDto.getUsername()).isPresent() || userElasticsearchRepository.findByUsername(userDto.getUsername()).isPresent())
            throw new UsernameAlreadyExists(userDto.getUsername());
        User newUser = userMapper.toUserDao(userDto);
        User user = userRepository.save(newUser);
        addUserToElasticsearch(user);
        return userDto;

    }

    private void addUserToElasticsearch(User user) {
        IndexQuery indexQuery = new IndexQueryBuilder()
                .withId(user.getId().toString())
                .withObject(user)
                .build();

        IndexCoordinates coord = elasticsearchOperations.getIndexCoordinatesFor(User.class);
        elasticsearchOperations.index(indexQuery, coord);
    }

    @Override
    public UserDto updateUser(String username, UserDto userDto) throws UserNotFoundException, UsernameAlreadyExists {
        if (userRepository.findByUsername(username).isPresent() && userElasticsearchRepository.findByUsername(username).isPresent()) {
            if(userRepository.findByUsername(userDto.getUsername()).isPresent() || userElasticsearchRepository.findByUsername(userDto.getUsername()).isPresent()) {
                throw new UsernameAlreadyExists(userDto.getUsername());
            }
            User user = userRepository.findByUsername(username).get();
            user.setFirstName(userDto.getFirstName());
            user.setLastName(userDto.getLastName());
            user.setUsername(userDto.getUsername());
            user.setPassword(userDto.getPassword());
            user.setUserRole(userDto.getUserRole());
            user.setEmail(userDto.getEmail());
            User newUser = userRepository.save(user);

            User oldUser = userElasticsearchRepository.findByUsername(username).get();
            userElasticsearchRepository.delete(oldUser);
            addUserToElasticsearch(newUser);

            return userDto;
        } else {
            throw new UserNotFoundException(username);
        }
    }

    @Override
    public void deleteUserByUsername(String username) throws UserNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        User user2 = userElasticsearchRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        userRepository.delete(user);
        userElasticsearchRepository.delete(user2);
    }

    @Override
    public UserDto loanBookUser(String username, String bookTitle) throws UserNotFoundException, BookNotFoundException, OutOfStockException {
        User retrievedUser = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        Book retrievedBook = bookRepository.findByTitle(bookTitle)
                .orElseThrow(() -> new BookNotFoundException(bookTitle));

        if (retrievedBook.getStock() > 0) {
            retrievedBook.setStock(retrievedBook.getStock() - 1);
            bookRepository.save(retrievedBook);

            Book book = bookElasticsearchRepository.findByTitle(bookTitle)
                    .orElseThrow(() -> new BookNotFoundException(bookTitle));
            bookElasticsearchRepository.delete(book);
            Book newBook = new Book();
            newBook.setTitle(retrievedBook.getTitle());
            newBook.setCategory(retrievedBook.getCategory());
            newBook.setPublisher(retrievedBook.getPublisher());
            newBook.setStock(retrievedBook.getStock());
            newBook.setUserRent(null);
            newBook.setAuthor(null);
            bookElasticsearchRepository.save(newBook);

            Set<Book> bookRent = retrievedUser.getBookRent();
            bookRent.add(bookRepository.findByTitle(bookTitle).get());
            retrievedUser.setBookRent(bookRent);
            userRepository.save(retrievedUser);

            return userMapper.toUserDto(retrievedUser);
        } else {
            throw new OutOfStockException(bookTitle);
        }

    }

    @Override
    public UserDto returnBookUser(String username, String bookTitle) throws UserNotFoundException, BookNotFoundException {
        User retrievedUser = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
        Book retrievedBook = bookRepository.findByTitle(bookTitle)
                .orElseThrow(() -> new BookNotFoundException(bookTitle));
        Set<Book> bookRent = retrievedUser.getBookRent();

        if (bookRent.contains(retrievedBook)) {
            retrievedBook.setStock(retrievedBook.getStock() + 1);
            bookRepository.save(retrievedBook);

            Book book = bookElasticsearchRepository.findByTitle(bookTitle)
                    .orElseThrow(() -> new BookNotFoundException(bookTitle));
            bookElasticsearchRepository.delete(book);
            Book newBook = new Book();
            newBook.setTitle(retrievedBook.getTitle());
            newBook.setCategory(retrievedBook.getCategory());
            newBook.setPublisher(retrievedBook.getPublisher());
            newBook.setStock(retrievedBook.getStock());
            newBook.setUserRent(null);
            newBook.setAuthor(null);
            bookElasticsearchRepository.save(newBook);

            bookRent.remove(bookRepository.findByTitle(bookTitle).get());
            retrievedUser.setBookRent(bookRent);
            userRepository.save(retrievedUser);

            return userMapper.toUserDto(retrievedUser);
        } else {
            throw new BookNotFoundException(bookTitle);
        }
    }

    @Override
    public Set<BookDto> getAllLoanedBooksForUser(String username) {
        User user = userRepository.findByUsername(username).get();
        return bookMapper.toBookDtoSet(user.getBookRent());
    }
}
