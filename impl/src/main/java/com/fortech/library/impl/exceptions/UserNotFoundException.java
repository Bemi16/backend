package com.fortech.library.impl.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String username) {
        super("Could not find the user with name: " + username);
    }
}
