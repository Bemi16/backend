package com.fortech.library.impl.services.impl;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.impl.dao.author.Author;
import com.fortech.library.impl.dao.author.AuthorRepository;
import com.fortech.library.impl.elasticsearch.AuthorElasticsearchRepository;
import com.fortech.library.impl.exceptions.AuthorAlreadyExists;
import com.fortech.library.impl.exceptions.AuthorNotFoundException;
import com.fortech.library.impl.mapper.author.api.AuthorMapper;
import com.fortech.library.impl.services.api.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorMapper authorMapper;
    private final AuthorRepository authorRepository;
    private final ElasticsearchOperations elasticsearchOperations;
    private final AuthorElasticsearchRepository authorElasticsearchRepository;

    @Autowired
    public AuthorServiceImpl(AuthorMapper authorMapper, AuthorRepository authorRepository, ElasticsearchOperations elasticsearchOperations, AuthorElasticsearchRepository authorElasticsearchRepository) {
        this.authorMapper = authorMapper;
        this.authorRepository = authorRepository;
        this.elasticsearchOperations = elasticsearchOperations;
        this.authorElasticsearchRepository = authorElasticsearchRepository;
    }

    @Override
    public List<Author> findAll() {
        return authorRepository.findAll();
    }

    @Override
    public AuthorDto getAuthorById(Integer id) throws AuthorNotFoundException {
        Author author = authorRepository.findById(id)
                .orElseThrow(() -> new AuthorNotFoundException(id.toString()));
        return authorMapper.toAuthorDto(author);
    }

    @Override
    public Page<AuthorDto> getAllAuthors(Integer pageNumber) {
        Pageable pageable = createPageRequest(pageNumber);

        Page<Author> authors = authorElasticsearchRepository.findAll(pageable);

        return new PageImpl<>(
                authors.getContent().stream()
                        .map(author -> authorMapper.toAuthorDto(author))
                        .collect(Collectors.toList()),
                pageable, authors.getTotalElements()
        );
    }

    private Pageable createPageRequest(Integer pageNumber) {
        return PageRequest.of(pageNumber, 20);
    }

    @Override
    public AuthorDto getAuthorByName(String authorName) throws AuthorNotFoundException {
        Author author = authorElasticsearchRepository.findByName(authorName)
                .orElseThrow(() -> new AuthorNotFoundException(authorName));
        return authorMapper.toAuthorDto(author);
    }

    @Override
    public AuthorDto addAuthor(AuthorDto authorDto) throws AuthorAlreadyExists {
        if (authorRepository.findByName(authorDto.getName()).isPresent() || authorElasticsearchRepository.findByName(authorDto.getName()).isPresent()) {
            throw new AuthorAlreadyExists(authorDto.getName());
        }

        Author author = authorRepository.save(authorMapper.toAuthorDao(authorDto));
        addAuthorToElasticsearch(author);
        return authorDto;
    }

    private void addAuthorToElasticsearch(Author author) {
        IndexQuery indexQuery = new IndexQueryBuilder()
                .withId(author.getId().toString())
                .withObject(author)
                .build();

        IndexCoordinates coord = elasticsearchOperations.getIndexCoordinatesFor(Author.class);
        elasticsearchOperations.index(indexQuery, coord);
    }

    @Override
    public AuthorDto updateAuthor(String authorName, AuthorDto authorDto) throws AuthorNotFoundException {
        if (authorRepository.findByName(authorName).isPresent() && authorElasticsearchRepository.findByName(authorName).isPresent()) {
            Author a1 = authorRepository.findByName(authorName).get();
            a1.setName(authorDto.getName());
            a1.setBiography(authorDto.getBiography());
            authorRepository.save(a1);

            Author a2 = authorElasticsearchRepository.findByName(authorName).get();
            authorElasticsearchRepository.delete(authorElasticsearchRepository.findByName(authorName).get());
            a2.setName(authorDto.getName());
            a2.setBiography(authorDto.getBiography());
            addAuthorToElasticsearch(a2);

            return authorDto;
        } else {
            throw new AuthorNotFoundException(authorName);
        }
    }

    @Override
    public void deleteAuthorByName(String authorName) throws AuthorNotFoundException {
        if (authorRepository.findByName(authorName).isPresent() && authorElasticsearchRepository.findByName(authorName).isPresent()) {
            authorRepository.delete(authorRepository.findByName(authorName).get());
            authorElasticsearchRepository.delete(authorElasticsearchRepository.findByName(authorName).get());
        } else {
            throw new AuthorNotFoundException(authorName);
        }
    }
}
