package com.fortech.library.impl.elasticsearch;

import com.fortech.library.impl.dao.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Optional;

public interface UserElasticsearchRepository extends ElasticsearchRepository<User, Integer> {

    Optional<User> findByUsername(String username);

    Page<User> findAll(Pageable pageable);
}
