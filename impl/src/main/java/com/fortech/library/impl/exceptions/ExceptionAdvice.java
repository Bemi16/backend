package com.fortech.library.impl.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionAdvice {
    @ResponseBody
    @ExceptionHandler(BookNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String bookNotFoundHandler(BookNotFoundException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String userNotFoundHandler(UserNotFoundException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(AuthorNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String authorNotFoundHandler(AuthorNotFoundException e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(UsernameAlreadyExists.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String usernameAlreadyExistsHandler(UsernameAlreadyExists e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(AuthorAlreadyExists.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String authorAlreadyExists(AuthorAlreadyExists e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(BookAlreadyExists.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String bookAlreadyExists(BookAlreadyExists e) {
        return e.getMessage();
    }

    @ResponseBody
    @ExceptionHandler(OutOfStockException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public String outOfStockHandler(OutOfStockException e) {
        return e.getMessage();
    }
}
