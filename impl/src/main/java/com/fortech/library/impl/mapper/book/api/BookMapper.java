package com.fortech.library.impl.mapper.book.api;

import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.impl.dao.book.Book;

import java.util.List;
import java.util.Set;

public interface BookMapper {

    BookDto toBookDto(Book book);

    Book toBookDao(BookDto bookDto);

    List<BookDto> toBookDtoList(List<Book> books);

    Set<BookDto> toBookDtoSet(Set<Book> books);
}
