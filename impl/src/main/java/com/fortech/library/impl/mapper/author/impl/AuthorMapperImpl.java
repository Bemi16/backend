package com.fortech.library.impl.mapper.author.impl;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.impl.dao.author.Author;
import com.fortech.library.impl.mapper.author.api.AuthorMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class AuthorMapperImpl implements AuthorMapper {

    @Override
    public AuthorDto toAuthorDto(Author author) {
        return new AuthorDto(author.getName(), author.getBiography());
    }

    @Override
    public Author toAuthorDao(AuthorDto authorDto) {
        Author author = new Author();
        author.setName(authorDto.getName());
        author.setBiography(authorDto.getBiography());
        return author;
    }

    @Override
    public List<AuthorDto> toAuthorDtoList(List<Author> authors) {
        return authors.stream().map(author -> new AuthorDto(author.getName(), author.getBiography()))
                .collect(Collectors.toList());
    }

}
