package com.fortech.library.impl.elasticsearch;

import com.fortech.library.impl.dao.book.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.Optional;

public interface BookElasticsearchRepository extends ElasticsearchRepository<Book, Integer> {

    Page<Book> findAll(Pageable pageable);

    Optional<Book> findByTitle(String title);
}
