package com.fortech.library.impl.elasticsearch;

import com.fortech.library.impl.dao.author.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;
import java.util.Optional;

public interface AuthorElasticsearchRepository extends ElasticsearchRepository<Author, Integer> {

    Page<Author> findAll(Pageable pageable);

    List<Author> findAll();

    Optional<Author> findByName(String name);
}
