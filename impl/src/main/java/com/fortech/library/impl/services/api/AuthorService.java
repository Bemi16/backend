package com.fortech.library.impl.services.api;


import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.impl.dao.author.Author;
import com.fortech.library.impl.exceptions.AuthorNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AuthorService {

    List<Author> findAll();

    Page<AuthorDto> getAllAuthors(Integer pageNumber);

    AuthorDto getAuthorById(Integer id) throws AuthorNotFoundException;

    AuthorDto getAuthorByName(String authorName) throws AuthorNotFoundException;

    AuthorDto addAuthor(AuthorDto authorDto);

    AuthorDto updateAuthor(String authorName, AuthorDto authorDto) throws AuthorNotFoundException;

    void deleteAuthorByName(String authorName) throws AuthorNotFoundException;
}
