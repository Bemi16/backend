package com.fortech.library.impl.services.impl;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.impl.dao.author.Author;
import com.fortech.library.impl.dao.author.AuthorRepository;
import com.fortech.library.impl.dao.book.Book;
import com.fortech.library.impl.dao.book.BookRepository;
import com.fortech.library.impl.elasticsearch.AuthorElasticsearchRepository;
import com.fortech.library.impl.elasticsearch.BookElasticsearchRepository;
import com.fortech.library.impl.exceptions.AuthorNotFoundException;
import com.fortech.library.impl.exceptions.BookAlreadyExists;
import com.fortech.library.impl.exceptions.BookNotFoundException;
import com.fortech.library.impl.mapper.author.api.AuthorMapper;
import com.fortech.library.impl.mapper.book.api.BookMapper;
import com.fortech.library.impl.services.api.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.IndexQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final BookMapper bookMapper;
    private final AuthorMapper authorMapper;
    private final AuthorRepository authorRepository;
    private final ElasticsearchOperations elasticsearchOperations;
    private final BookElasticsearchRepository bookElasticsearchRepository;
    private final AuthorElasticsearchRepository authorElasticsearchRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository, BookMapper bookMapper, AuthorMapper authorMapper, AuthorRepository authorRepository, ElasticsearchOperations elasticsearchOperations, BookElasticsearchRepository bookElasticsearchRepository, AuthorElasticsearchRepository authorElasticsearchRepository) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
        this.authorMapper = authorMapper;
        this.authorRepository = authorRepository;
        this.elasticsearchOperations = elasticsearchOperations;
        this.bookElasticsearchRepository = bookElasticsearchRepository;
        this.authorElasticsearchRepository = authorElasticsearchRepository;
    }

    @Override
    public List<BookDto> getAllBooks(String authorName) {
        Author author = authorRepository.findByName(authorName).get();
        List<Book> books = author.getBookList();
        return bookMapper.toBookDtoList(books);
    }

    @Override
    public BookDto addBook(BookDto bookDto, String authorName) throws AuthorNotFoundException, BookAlreadyExists {
        Author author = authorRepository.findByName(authorName)
                .orElseThrow(() -> new AuthorNotFoundException(authorName));
        if (bookElasticsearchRepository.findByTitle(bookDto.getTitle()).isPresent() || bookRepository.findByTitle(bookDto.getTitle()).isPresent()) {
            throw new BookAlreadyExists(bookDto.getTitle());
        }
        Book book = bookMapper.toBookDao(bookDto);
        book.setAuthor(author);
        book = bookRepository.save(book);
        book.setAuthor(null);
        addBookToElasticsearch(book);
        return bookDto;
    }

    private void addBookToElasticsearch(Book book) {
        IndexQuery indexQuery = new IndexQueryBuilder()
                .withId(book.getId().toString())
                .withObject(book)
                .build();

        IndexCoordinates coord = elasticsearchOperations.getIndexCoordinatesFor(Book.class);
        elasticsearchOperations.index(indexQuery, coord);
    }

    @Override
    public BookDto updateBookForAuthor(String authorName, String bookTitle, BookDto bookDto) throws AuthorNotFoundException {
        Author author = authorRepository.findByName(authorName)
                .orElseThrow(() -> new AuthorNotFoundException(authorName));
        if (bookRepository.findByTitle(bookTitle).isPresent() && bookElasticsearchRepository.findByTitle(bookTitle).isPresent()) {
            Book book = bookRepository.findByTitle(bookTitle).get();
            book.setCategory(bookDto.getCategory());
            book.setPublisher(bookDto.getPublisher());
            book.setTitle(bookDto.getTitle());
            book.setStock(bookDto.getStock());
            book.setAuthor(author);
            book = bookRepository.save(book);
            book.setAuthor(null);

            bookElasticsearchRepository.delete(bookElasticsearchRepository.findByTitle(bookTitle).get());
            addBookToElasticsearch(book);
            return bookDto;

        } else {
            return addBook(bookDto, authorName);
        }
    }

    @Override
    public void deleteBookByTitle(String bookTitle) throws BookNotFoundException {
        if (bookRepository.findByTitle(bookTitle).isPresent() && bookElasticsearchRepository.findByTitle(bookTitle).isPresent()) {
            bookRepository.delete(bookRepository.findByTitle(bookTitle).get());
            bookElasticsearchRepository.delete(bookElasticsearchRepository.findByTitle(bookTitle).get());
        } else {
            throw new BookNotFoundException(bookTitle);
        }

    }

    @Override
    public Page<BookDto> getAllBooksForAllAuthors(Integer pageNumber) {
        Pageable pageable = createPageRequest(pageNumber);

        Page<Book> books = bookElasticsearchRepository.findAll(pageable);

        return new PageImpl<>(
                books.getContent().stream()
                        .map(b -> bookMapper.toBookDto(b))
                        .collect(Collectors.toList()),
                pageable, books.getTotalElements()
        );
    }

    @Override
    public AuthorDto getAuthorForBook(String bookTitle) throws BookNotFoundException{
        Book book = bookRepository.findByTitle(bookTitle)
                .orElseThrow(() -> new BookNotFoundException(bookTitle));
        if(book.getAuthor() == null){
            throw new AuthorNotFoundException("Annonymous");
        }
        return authorMapper.toAuthorDto(book.getAuthor());
    }

    private Pageable createPageRequest(Integer pageNumber) {
        return PageRequest.of(pageNumber, 20);
    }
}
