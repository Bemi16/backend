package com.fortech.library.impl.mapper.user.api;

import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.dao.user.User;

import java.util.List;

public interface UserMapper {
    UserDto toUserDto(User user);

    User toUserDao(UserDto userDto);

    List<UserDto> toUserDtoList(List<User> users);

}
