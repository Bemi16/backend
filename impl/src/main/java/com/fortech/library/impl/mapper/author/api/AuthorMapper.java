package com.fortech.library.impl.mapper.author.api;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.impl.dao.author.Author;

import java.util.List;

public interface AuthorMapper {

    AuthorDto toAuthorDto(Author author);

    Author toAuthorDao(AuthorDto authorDto);

    List<AuthorDto> toAuthorDtoList(List<Author> authors);

}
