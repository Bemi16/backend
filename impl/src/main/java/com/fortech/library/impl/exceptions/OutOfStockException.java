package com.fortech.library.impl.exceptions;

public class OutOfStockException extends RuntimeException {
    public OutOfStockException(String bookTitle) {
        super("The book with the title " + bookTitle + " is currently out of stock.");
    }
}
