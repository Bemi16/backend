package com.fortech.library.impl.controllers;

import com.fortech.library.api.controllers.UserController;
import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.services.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class UserControllerImpl implements UserController {

    private final UserService userService;

    @Autowired
    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public ResponseEntity<Page<UserDto>> getAllUsers(Integer pageNumber) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getAllUsers(pageNumber));
    }

    @Override
    public ResponseEntity<UserDto> getUserByUsername(String username) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getUserByUsername(username));
    }

    @Override
    public ResponseEntity<UserDto> createUser(UserDto user) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.addUser(user));
    }

    @Override
    public ResponseEntity<String> deleteUserByUsername(String username) {
        userService.deleteUserByUsername(username);
        return ResponseEntity.status(HttpStatus.OK).body("Deleted User.");
    }

    @Override
    public ResponseEntity<UserDto> updateUser(String username, UserDto user) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.updateUser(username, user));
    }

    @Override
    public ResponseEntity<UserDto> loanBookUser(String username, String bookTitle) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.loanBookUser(username, bookTitle));
    }

    @Override
    public ResponseEntity<UserDto> returnBookUser(String username, String bookTitle) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.returnBookUser(username, bookTitle));
    }

    @Override
    public ResponseEntity<Set<BookDto>> getAllLoanedBooksForUser(String username) {
        return ResponseEntity.status(HttpStatus.OK).body(userService.getAllLoanedBooksForUser(username));
    }
}
