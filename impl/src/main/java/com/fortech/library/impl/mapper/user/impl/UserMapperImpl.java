package com.fortech.library.impl.mapper.user.impl;

import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.dao.user.User;
import com.fortech.library.impl.mapper.user.api.UserMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapperImpl implements UserMapper {
    @Override
    public UserDto toUserDto(User user) {
        return new UserDto(user.getFirstName(), user.getLastName(), user.getUsername(), user.getPassword(), user.getUserRole(), user.getEmail());
    }

    @Override
    public User toUserDao(UserDto userDto) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setUserRole(userDto.getUserRole());
        user.setPassword(userDto.getPassword());
        user.setUsername(userDto.getUsername());
        user.setLastName(userDto.getLastName());
        user.setFirstName(userDto.getFirstName());
        return user;
    }

    @Override
    public List<UserDto> toUserDtoList(List<User> users) {
        return users.stream().map(user -> new UserDto(user.getFirstName(), user.getLastName(), user.getUsername(), user.getPassword(), user.getUserRole(), user.getEmail()))
                .collect(Collectors.toList());
    }


}
