package com.fortech.library.impl.controllers;

import com.fortech.library.api.controllers.AuthorController;
import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.impl.services.api.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthorControllerImpl implements AuthorController {

    private final AuthorService authorService;

    @Autowired
    public AuthorControllerImpl(AuthorService authorService) {
        this.authorService = authorService;
    }

    @Override
    public ResponseEntity<Page<AuthorDto>> getAllAuthors(Integer pageNumber) {
        return ResponseEntity.ok(authorService.getAllAuthors(pageNumber));
    }

    @Override
    public ResponseEntity<AuthorDto> getAuthorByName(String authorName) {
        return ResponseEntity.status(HttpStatus.OK).body(authorService.getAuthorByName(authorName));
    }

    @Override
    public ResponseEntity<AuthorDto> addAuthor(AuthorDto authorDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(authorService.addAuthor(authorDto));
    }

    @Override
    public ResponseEntity<String> deleteAuthorByName(String authorName) {
        authorService.deleteAuthorByName(authorName);
        return ResponseEntity.status(HttpStatus.OK).body("Deleted Author.");
    }

    @Override
    public ResponseEntity<AuthorDto> updateAuthor(String authorName, AuthorDto authorDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(authorService.updateAuthor(authorName, authorDto));
    }
}
