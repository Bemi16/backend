package com.fortech.library.impl.controllers;

import com.fortech.library.api.controllers.BookController;
import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.impl.services.api.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookControllerImpl implements BookController {

    private final BookService bookService;

    @Autowired
    public BookControllerImpl(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public ResponseEntity<BookDto> addBookToAuthor(BookDto book, String authorName) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.addBook(book, authorName));
    }

    @Override
    public ResponseEntity<Page<BookDto>> getAllBooksForAllAuthors(Integer pageNumber) {
        return ResponseEntity.status(HttpStatus.OK).body(bookService.getAllBooksForAllAuthors(pageNumber));
    }

    @Override
    public ResponseEntity<List<BookDto>> getAllBooksForAuthor(String authorName) {
        return ResponseEntity.ok(bookService.getAllBooks(authorName));
    }

    @Override
    public ResponseEntity<AuthorDto> getAuthorForBook(String bookTitle) {
        return ResponseEntity.ok(bookService.getAuthorForBook(bookTitle));
    }

    @Override
    public ResponseEntity<BookDto> updateBookForAuthor(String authorName, String bookTitle, BookDto book) {
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.updateBookForAuthor(authorName, bookTitle, book));
    }

    @Override
    public ResponseEntity<String> deleteBookByTitleFromAuthor(String bookTitle) {
        bookService.deleteBookByTitle(bookTitle);
        return ResponseEntity.status(HttpStatus.OK).body("Deleted Book.");
    }
}
