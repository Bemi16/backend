package com.fortech.library.impl.dao.user;

import com.fortech.library.impl.dao.book.Book;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
@Document(indexName = "users")
public class User {
    private @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue
    Integer id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String userRole;
    private String email;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "users_books",
            joinColumns = {@JoinColumn(name = "u_id")},
            inverseJoinColumns = {@JoinColumn(name = "b_id")}
    )
    private Set<Book> bookRent;

    public User() {
    }

    public User(String firstName, String lastName, String username, String password, String userRole, String email, Set<Book> bookRent) {
        this.bookRent = bookRent;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.userRole = userRole;
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String fName) {
        this.firstName = fName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lName) {
        this.lastName = lName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Book> getBookRent() {
        return bookRent;
    }

    public void setBookRent(Set<Book> bookList) {
        this.bookRent = bookList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(username, user.username) &&
                Objects.equals(password, user.password) &&
                Objects.equals(userRole, user.userRole) &&
                Objects.equals(email, user.email) &&
                Objects.equals(bookRent, user.bookRent);
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", userRole='" + userRole + '\'' +
                ", email='" + email + '\'' +
                ", bookList=" + bookRent +
                '}';
    }
}
