package com.fortech.library.impl.exceptions;

public class AuthorAlreadyExists extends RuntimeException {
    public AuthorAlreadyExists(String name) {
        super("Author with name " + name + " already exists!");
    }
}
