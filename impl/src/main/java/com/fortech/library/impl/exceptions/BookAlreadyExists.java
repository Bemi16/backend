package com.fortech.library.impl.exceptions;

public class BookAlreadyExists extends RuntimeException {
    public BookAlreadyExists(String title) {
        super("Book with title " + title + " already exists");
    }
}
