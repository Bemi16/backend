package com.fortech.library.impl.exceptions;

public class AuthorNotFoundException extends RuntimeException {
    public AuthorNotFoundException(String name) {
        super("Could not find the Author with the name: " + name);
    }
}
