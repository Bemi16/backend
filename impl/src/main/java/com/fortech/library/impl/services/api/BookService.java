package com.fortech.library.impl.services.api;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.impl.exceptions.AuthorNotFoundException;
import com.fortech.library.impl.exceptions.BookNotFoundException;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BookService {

    List<BookDto> getAllBooks(String authorName);

    BookDto addBook(BookDto bookDto, String authorName) throws AuthorNotFoundException;

    BookDto updateBookForAuthor(String authorName, String bookTitle, BookDto book) throws BookNotFoundException;

    void deleteBookByTitle(String bookTitle) throws BookNotFoundException;

    Page<BookDto> getAllBooksForAllAuthors(Integer pageNumber);

    AuthorDto getAuthorForBook(String bookTitle);

}
