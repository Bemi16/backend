package com.fortech.library.impl.dao.book;

import com.fortech.library.impl.dao.author.Author;
import com.fortech.library.impl.dao.user.User;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Document(indexName = "books")
@Entity
@Table(name = "Books")
public class Book {
    private @Id
    @org.springframework.data.annotation.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    private String title;
    private String publisher;
    private String category;
    private int stock;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "a_id")
    private Author author;

    @ManyToMany(mappedBy = "bookRent")
    private Set<User> userRent;

    public Book() {
    }

    public Book(String title, String publisher, String category, int stock, Author author, Set<User> userRent) {
        this.title = title;
        this.publisher = publisher;
        this.category = category;
        this.stock = stock;
        this.author = author;
        this.userRent = userRent;
    }

    public Book(String title, String publisher, String category) {
        this.title = title;
        this.publisher = publisher;
        this.category = category;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Set<User> getUserRent() {
        return userRent;
    }

    public void setUserRent(Set<User> userRent) {
        this.userRent = userRent;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return stock == book.stock &&
                Objects.equals(id, book.id) &&
                Objects.equals(title, book.title) &&
                Objects.equals(publisher, book.publisher) &&
                Objects.equals(category, book.category) &&
                Objects.equals(author, book.author) &&
                Objects.equals(userRent, book.userRent);
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", publisher='" + publisher + '\'' +
                ", category='" + category + '\'' +
                ", stock=" + stock +
                ", author=" + author +
                ", userRent=" + userRent +
                '}';
    }
}
