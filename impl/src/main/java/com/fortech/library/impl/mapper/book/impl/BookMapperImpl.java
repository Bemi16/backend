package com.fortech.library.impl.mapper.book.impl;

import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.impl.dao.book.Book;
import com.fortech.library.impl.mapper.book.api.BookMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BookMapperImpl implements BookMapper {

    @Override
    public BookDto toBookDto(Book book) {
        return new BookDto(book.getTitle(), book.getCategory(), book.getPublisher(), book.getStock());
    }

    @Override
    public Book toBookDao(BookDto bookDto) {
        Book book = new Book();
        book.setStock(bookDto.getStock());
        book.setTitle(bookDto.getTitle());
        book.setPublisher(bookDto.getPublisher());
        book.setCategory(bookDto.getCategory());
        return book;
    }

    @Override
    public List<BookDto> toBookDtoList(List<Book> books) {
        return books.stream().map(book -> new BookDto(book.getTitle(), book.getCategory(), book.getPublisher(), book.getStock()))
                .collect(Collectors.toList());
    }

    @Override
    public Set<BookDto> toBookDtoSet(Set<Book> books) {
        return books.stream().map(book -> new BookDto(book.getTitle(), book.getCategory(), book.getPublisher(), book.getStock()))
                .collect(Collectors.toSet());
    }

}
