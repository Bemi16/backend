package com.fortech.library.impl.services.api;

import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.exceptions.BookNotFoundException;
import com.fortech.library.impl.exceptions.OutOfStockException;
import com.fortech.library.impl.exceptions.UserNotFoundException;
import org.springframework.data.domain.Page;

import java.util.Set;

public interface UserService {

    Page<UserDto> getAllUsers(Integer pageNumber);

    UserDto getUserByUsername(String username) throws UserNotFoundException;

    UserDto addUser(UserDto userDto);

    UserDto updateUser(String username, UserDto userDto) throws UserNotFoundException;

    void deleteUserByUsername(String username) throws UserNotFoundException;

    UserDto loanBookUser(String username, String bookTitle) throws UserNotFoundException, BookNotFoundException, OutOfStockException;

    UserDto returnBookUser(String username, String bookTitle) throws UserNotFoundException, BookNotFoundException;

    Set<BookDto> getAllLoanedBooksForUser(String username);

}
