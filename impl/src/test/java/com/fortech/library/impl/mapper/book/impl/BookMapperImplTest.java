package com.fortech.library.impl.mapper.book.impl;

import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.impl.dao.book.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class BookMapperImplTest {

    private BookMapperImpl bookMapper;

    @BeforeEach
    public void setUp() {
        bookMapper = new BookMapperImpl();
    }

    @Test
    void withGivenBookDao_shouldReturnBookDto() {
        Book book = new Book("A", "B", "C", 1, null, null);
        BookDto bookDto = bookMapper.toBookDto(book);

        assertEquals(book.getTitle(), bookDto.getTitle());
    }

    @Test
    void withGivenBookDto_shouldReturnBookDao() {
        BookDto bookDto = new BookDto("A", "B", "C", 5);
        Book book = bookMapper.toBookDao(bookDto);

        assertEquals(book.getTitle(), bookDto.getTitle());
    }

    @Test
    void withGivenBookDaoList_shouldReturnBookDtoList() {
        List<Book> expectedList = Arrays.asList(new Book("A", "B", "C", 2, null, null));

        List<BookDto> actualList = bookMapper.toBookDtoList(expectedList);

        assertEquals(actualList.size(), expectedList.size());
    }
}