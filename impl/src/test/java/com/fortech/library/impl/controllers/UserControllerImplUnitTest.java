package com.fortech.library.impl.controllers;

import com.fortech.library.api.controllers.UserController;
import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.services.api.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserControllerImplUnitTest {

    @Mock
    private UserService userService;
    private UserController userController;

    @BeforeEach
    void init() {
        userService = mock(UserService.class);
        userController = new UserControllerImpl(userService);
    }

    @Test
    void getAllUsers_callsServiceMethod() {
        userController.getAllUsers(any());
        verify(userService, times(1)).getAllUsers(any());
    }

    @Test
    void getUserByUsername_callsServiceMethod() {
        userController.getUserByUsername("1");
        verify(userService, times(1)).getUserByUsername("1");
    }

    @Test
    void createUser_callsServiceMethod() {
        UserDto user = new UserDto();
        userController.createUser(user);
        verify(userService, times(1)).addUser(user);
    }

    @Test
    void deleteUser_callsServiceMethod() {
        userController.deleteUserByUsername("1");
        verify(userService, times(1)).deleteUserByUsername("1");
    }

    @Test
    void updateUser_callsServiceMethod() {
        UserDto user = new UserDto();
        userController.updateUser("1", user);
        verify(userService, times(1)).updateUser("1", user);
    }

    @Test
    void loanBookUser_callsServiceMethod() {
        userController.loanBookUser("1", "1");
        verify(userService, times(1)).loanBookUser("1", "1");
    }

    @Test
    void returnBookUser_callsServiceMethod() {
        userController.returnBookUser("1", "1");
        verify(userService, times(1)).returnBookUser("1", "1");
    }

    @Test
    void getAllLoanedBooksForUser_callsServiceMethod() {
        userController.getAllLoanedBooksForUser("1");
        verify(userService, times(1)).getAllLoanedBooksForUser("1");
    }
}
