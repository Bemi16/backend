package com.fortech.library.impl.services.impl;

import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.dao.user.User;
import com.fortech.library.impl.dao.user.UserRepository;
import com.fortech.library.impl.elasticsearch.UserElasticsearchRepository;
import com.fortech.library.impl.exceptions.UserNotFoundException;
import com.fortech.library.impl.mapper.user.impl.UserMapperImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private UserMapperImpl userMapper;
    @Mock
    private UserElasticsearchRepository userElasticsearchRepository;

    @Test
    void withGiven_shouldReturnUserPage() {
        UserDto expectedUser = new UserDto("Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy");
        Pageable pageable = PageRequest.of(0, 20);
        Page<User> expected = new PageImpl<>(
                Arrays.asList(new User("Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", null)),
                pageable, 1
        );
        when(userElasticsearchRepository.findAll(any(Pageable.class))).thenReturn(expected);
        when(userMapper.toUserDto(any())).thenReturn(expectedUser);

        Page<UserDto> actual = userService.getAllUsers(0);

        assertThat(actual.getTotalElements()).isEqualTo(1);
        assertThat(actual.stream().filter(u -> u.getFirstName().equals("Dummy")).findAny()).isEqualTo(Optional.of(expectedUser));
    }

    @Test
    void withGivenUserName_shouldReturnUser() {
        when(userElasticsearchRepository.findByUsername(any())).thenReturn(Optional.of(new User()));
        when(userMapper.toUserDto(any())).thenReturn(new UserDto("Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy"));

        UserDto actual = userService.getUserByUsername(any());

        assertThat(actual.getFirstName()).isEqualTo("Dummy");
    }

    @Test
    void withGivenUsername_shouldReturnUserNotFoundException() {
        when(userElasticsearchRepository.findByUsername(any())).thenReturn(Optional.empty());

        Exception ex = assertThrows(UserNotFoundException.class, () -> userService.getUserByUsername(any()));

        assertTrue(ex.getMessage().contains("Could not find"));
    }

    @Test
    void withGivenUsername_shouldDeleteUser() {
        when(userRepository.findByUsername(any())).thenReturn(Optional.of(new User()));
        when(userElasticsearchRepository.findByUsername(any())).thenReturn(Optional.of(new User()));

        userService.deleteUserByUsername(any());

        verify(userRepository, times(1)).delete(any());
        verify(userElasticsearchRepository, times(1)).delete(any());
    }
}
