package com.fortech.library.impl.services.impl;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.impl.dao.author.Author;
import com.fortech.library.impl.dao.author.AuthorRepository;
import com.fortech.library.impl.elasticsearch.AuthorElasticsearchRepository;
import com.fortech.library.impl.exceptions.AuthorNotFoundException;
import com.fortech.library.impl.mapper.author.api.AuthorMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthorServiceImplTest {

    @Mock
    private AuthorRepository authorRepository;

    @Mock
    private AuthorMapper authorMapper;

    @Mock
    private AuthorElasticsearchRepository authorElasticsearchRepository;

    @Mock
    private ElasticsearchOperations elasticsearchOperations;

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Test
    void whenGivenAuthorPage_shouldReturnAuthorDtoPage() {
        Pageable pageable = PageRequest.of(0, 20);
        Author expectedAuthor = new Author("Dummy", "Dummy");
        Page<Author> expected = new PageImpl<>(
                Arrays.asList(expectedAuthor),
                pageable, 1);
        when(authorElasticsearchRepository.findAll(any(Pageable.class))).thenReturn(expected);
        when(authorMapper.toAuthorDto(any())).thenReturn(new AuthorDto("Dummy", "Dummy"));

        Page<AuthorDto> actual = authorService.getAllAuthors(0);

        assertThat(actual.getTotalElements()).isEqualTo(expected.getTotalElements());
        assertThat(actual.stream()
                .filter(a -> a.getName().equals("Dummy"))
                .collect(Collectors.toList())).hasSize(1);
    }

    @Test
    void whenGivenName_shouldReturnAuthor() {
        Author expected = new Author("Dummy", "Dummy");
        when(authorElasticsearchRepository.findByName(any())).thenReturn(Optional.of(expected));
        when(authorMapper.toAuthorDto(any())).thenReturn(new AuthorDto("Dummy", "Dummy"));

        AuthorDto actual = authorService.getAuthorByName(any());

        assertThat(actual.getName()).isEqualTo("Dummy");
    }

    @Test
    void withGivenName_shoudThrowAuthorNotFoundException() {
        when(authorElasticsearchRepository.findByName(any())).thenReturn(Optional.empty());

        Exception ex = assertThrows(AuthorNotFoundException.class, () -> authorService.getAuthorByName(any()));

        assertTrue(ex.getMessage().contains("Could not find"));
    }

    @Test
    void withGivenAuthorDto_shouldReturnAuthorDto() {
        when(authorRepository.findByName(any())).thenReturn(Optional.empty());
        when(authorElasticsearchRepository.findByName(any())).thenReturn(Optional.empty());
        Author a = new Author();
        a.setId(1);
        when(authorRepository.save(any())).thenReturn(a);
        when(elasticsearchOperations.getIndexCoordinatesFor(any())).thenReturn(null);
        when(elasticsearchOperations.index(any(), any())).thenReturn(null);

        AuthorDto actual = authorService.addAuthor(new AuthorDto("Dummy", "Dummy"));

        assertThat(actual.getName()).isEqualTo("Dummy");
    }


    @Test
    void withGivenName_shouldDelete() {
        when(authorRepository.findByName(any())).thenReturn(Optional.of(new Author()));
        when(authorElasticsearchRepository.findByName(any())).thenReturn(Optional.of(new Author()));

        authorService.deleteAuthorByName(any());

        verify(authorRepository, times(1)).delete(any());
        verify(authorElasticsearchRepository, times(1)).delete(any());
    }
}
