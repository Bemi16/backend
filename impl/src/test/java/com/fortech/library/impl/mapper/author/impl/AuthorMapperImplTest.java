package com.fortech.library.impl.mapper.author.impl;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.impl.dao.author.Author;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AuthorMapperImplTest {

    private AuthorMapperImpl authorMapper;


    @BeforeEach
    public void setUp() {
        authorMapper = new AuthorMapperImpl();
    }

    @Test
    void withGivenAuthorDao_shouldReturnAuthorDto() {
        Author author = new Author("A", "C", null);
        AuthorDto authorDto = authorMapper.toAuthorDto(author);


        assertEquals(author.getName(), authorDto.getName());
    }

    @Test
    void withGivenAuthorDto_shouldReturnAuthorDao() {
        AuthorDto authorDto = new AuthorDto("a", "c");
        Author author = authorMapper.toAuthorDao(authorDto);


        assertEquals(authorDto.getName(), author.getName());
    }

    @Test
    void withGivenAuthorDaoList_shouldReturnAuthorDtoList() {
        List<Author> expectedList = Arrays.asList(new Author("a", "c", null));

        List<AuthorDto> actualList = authorMapper.toAuthorDtoList(expectedList);

        assertEquals(actualList.size(), expectedList.size());
    }
}