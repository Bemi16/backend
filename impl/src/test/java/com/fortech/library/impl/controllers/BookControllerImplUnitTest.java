package com.fortech.library.impl.controllers;

import com.fortech.library.api.controllers.BookController;
import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.impl.services.api.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookControllerImplUnitTest {

    @Mock
    private BookService bookService;
    private BookController bookController;

    @BeforeEach
    void init() {
        bookService = mock(BookService.class);
        bookController = new BookControllerImpl(bookService);
    }

    @Test
    void getAllBooksForAuthor_callsServiceMethod() {
        bookController.getAllBooksForAuthor("Name");
        verify(bookService, times(1)).getAllBooks("Name");
    }

    @Test
    void getAllBooksForAllAuthors_callsServiceMethod() {
        bookController.getAllBooksForAllAuthors(any());
        verify(bookService, times(1)).getAllBooksForAllAuthors(any());
    }

    @Test
    void createBook_callsServiceMethod() {
        BookDto book = new BookDto();
        bookController.addBookToAuthor(book, "Name");
        verify(bookService, times(1)).addBook(book, "Name");
    }

    @Test
    void deleteBook_callsServiceMethod() {
        bookController.deleteBookByTitleFromAuthor("1");
        verify(bookService, times(1)).deleteBookByTitle("1");
    }

    @Test
    void updateBook_callsServiceMethod() {
        BookDto book = new BookDto();
        bookController.updateBookForAuthor("1", "1", book);
        verify(bookService, times(1)).updateBookForAuthor("1", "1", book);
    }
}
