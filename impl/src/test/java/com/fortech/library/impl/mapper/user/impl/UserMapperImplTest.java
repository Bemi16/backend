package com.fortech.library.impl.mapper.user.impl;

import com.fortech.library.api.dto.user.UserDto;
import com.fortech.library.impl.dao.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserMapperImplTest {

    private UserMapperImpl userMapper;

    @BeforeEach
    public void setUp() {
        userMapper = new UserMapperImpl();
    }

    @Test
    void withGivenUserDao_shouldReturnUserDto() {
        User user = new User("A", "B", "C", "D", "E", "F", null);
        UserDto userDto = userMapper.toUserDto(user);

        assertEquals(user.getUsername(), userDto.getUsername());
    }

    @Test
    void withGivenUserDto_shouldReturnUserDao() {
        UserDto userDto = new UserDto("A", "B", "C", "D", "E", "F");
        User user = userMapper.toUserDao(userDto);

        assertEquals(user.getUsername(), userDto.getUsername());
    }

    @Test
    void withGivenUserDaoList_shouldReturnUserDtoList() {
        List<User> expectedList = Arrays.asList(new User("a", "b", "c", "d", "f", "g", null));

        List<UserDto> actualList = userMapper.toUserDtoList(expectedList);

        assertEquals(actualList.size(), expectedList.size());
    }

}