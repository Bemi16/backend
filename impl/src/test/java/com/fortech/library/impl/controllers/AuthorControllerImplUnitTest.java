package com.fortech.library.impl.controllers;

import com.fortech.library.api.dto.author.AuthorDto;
import com.fortech.library.impl.services.api.AuthorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class AuthorControllerImplUnitTest {

    @Mock
    private AuthorService authorService;
    private AuthorControllerImpl authorController;

    @BeforeEach
    public void setUp() {
        authorService = mock(AuthorService.class);
        authorController = new AuthorControllerImpl(authorService);
    }

    @Test
    void getAllAuthors_callsServiceMethod() {
        authorController.getAllAuthors(any());
        verify(authorService, times(1)).getAllAuthors(any());
    }

    @Test
    void createAuthor_callsServiceMethod() {
        AuthorDto author = new AuthorDto();
        authorController.addAuthor(author);
        verify(authorService, times(1)).addAuthor(author);
    }

    @Test
    void getAuthorByName_callsServiceMethod() {
        authorController.getAuthorByName("Name");
        verify(authorService, times(1)).getAuthorByName("Name");
    }

    @Test
    void deleteAuthor_callsServiceMethod() {
        authorController.deleteAuthorByName("Name");
        verify(authorService, times(1)).deleteAuthorByName("Name");
    }

    @Test
    void updateAuthor_callsServiceMethod() {
        AuthorDto author = new AuthorDto();
        authorController.updateAuthor("Name", author);
        verify(authorService, times(1)).updateAuthor("Name", author);
    }
}

