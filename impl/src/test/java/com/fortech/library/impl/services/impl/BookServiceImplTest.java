package com.fortech.library.impl.services.impl;

import com.fortech.library.api.dto.book.BookDto;
import com.fortech.library.impl.dao.author.Author;
import com.fortech.library.impl.dao.author.AuthorRepository;
import com.fortech.library.impl.dao.book.Book;
import com.fortech.library.impl.dao.book.BookRepository;
import com.fortech.library.impl.elasticsearch.BookElasticsearchRepository;
import com.fortech.library.impl.exceptions.BookNotFoundException;
import com.fortech.library.impl.mapper.book.impl.BookMapperImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;

import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookServiceImplTest {

    @Mock
    private AuthorRepository authorRepository;
    @Mock
    private BookRepository bookRepository;
    @Mock
    private BookMapperImpl bookMapper;
    @Mock
    private BookElasticsearchRepository bookElasticsearchRepository;
    @Mock
    private ElasticsearchOperations elasticsearchOperations;
    @InjectMocks
    private BookServiceImpl bookService;

    @Test
    void withGiven_shouldReturnBookPage() {
        BookDto expectedBook = new BookDto("Dummy", "Dummy", "Dummy", 1);
        Pageable pageable = PageRequest.of(0, 20);
        Page<Book> expected = new PageImpl<>(
                Arrays.asList(new Book("Dummy", "Dummy", "Dummy")),
                pageable, 1
        );
        when(bookElasticsearchRepository.findAll(any(Pageable.class))).thenReturn(expected);
        when(bookMapper.toBookDto(any())).thenReturn(expectedBook);

        Page<BookDto> actual = bookService.getAllBooksForAllAuthors(0);

        assertThat(actual.getTotalElements()).isEqualTo(1);
        assertThat(actual.stream().filter(b -> b.getTitle().equals("Dummy")).findAny()).isEqualTo(Optional.of(expectedBook));
    }

    @Test
    void withGivenBookDto_shouldReturnBookDto() {
        when(authorRepository.findByName(any())).thenReturn(Optional.of(new Author("Dummy", "Dummy")));
        when(bookElasticsearchRepository.findByTitle(any())).thenReturn(Optional.empty());
        when(bookRepository.findByTitle(any())).thenReturn(Optional.empty());
        Book b = new Book();
        b.setId(1);
        when(bookMapper.toBookDao(any())).thenReturn(new Book());
        when(bookRepository.save(any())).thenReturn(b);
        when(elasticsearchOperations.getIndexCoordinatesFor(any())).thenReturn(null);
        when(elasticsearchOperations.index(any(), any())).thenReturn(null);

        BookDto actual = bookService.addBook(new BookDto("Dummy", "Dummy", "Dummy", 1), any());

        assertThat(actual.getTitle()).isEqualTo("Dummy");
    }

    @Test
    void withGivenName_shouldDeleteBook() {
        when(bookRepository.findByTitle(any())).thenReturn(Optional.of(new Book()));
        when(bookElasticsearchRepository.findByTitle(any())).thenReturn(Optional.of(new Book()));

        bookService.deleteBookByTitle(any());

        verify(bookRepository, times(1)).delete(any());
        verify(bookElasticsearchRepository, times(1)).delete(any());
    }

    @Test
    void withGivenTitle_shouldThrowBookNotFoundException() {
        when(bookRepository.findByTitle(any())).thenReturn(Optional.empty());

        Exception ex = assertThrows(BookNotFoundException.class, () -> bookService.deleteBookByTitle(any()));

        assertTrue(ex.getMessage().contains("Could not find the book"));
    }
}
